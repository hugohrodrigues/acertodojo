﻿using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        public void AtualizarQualidade()
        {
            for (var i = 0; i < Itens.Count; i++)
            {
               
                if (Itens[i].Nome == "Queijo Brie Envelhecido" || Itens[i].Nome == "Ingressos para o concerto do Turisas")
                {
                    if (Itens[i].Nome == "Queijo Brie Envelhecido" )
                    {
                        Itens[i].Qualidade = (Itens[i].Qualidade <= 50) ? Itens[i].Qualidade + 1 : 50;
                     
                    }
                    else if (Itens[i].PrazoParaVenda <= 10 )
                    {
                        Itens[i].Qualidade = (Itens[i].Qualidade <= 50) ? Itens[i].Qualidade + 2 : 50;
                  
                       
                    }
                    else if (Itens[i].PrazoParaVenda <= 5)
                    {
                        Itens[i].Qualidade = (Itens[i].Qualidade <= 50) ? Itens[i].Qualidade + 3 : 50;

                    }
                    else
                    {
                        Itens[i].Qualidade = (Itens[i].PrazoParaVenda > 10) ? Itens[i].Qualidade + 1  : 0;
                   
                 
                    }
                }
                else if (Itens[i].Nome == "Bolo de Mana Conjurado")
                {
                 
                    Itens[i].Qualidade = (Itens[i].Qualidade <= 50) ? Itens[i].Qualidade - 2 : 50;

                }
                else
                {
                    Itens[i].Qualidade = (Itens[i].PrazoParaVenda > Itens[i].Qualidade) ? Itens[i].Qualidade - 2 : Itens[i].Qualidade - 1;
            

                }
                Itens[i].Qualidade = (Itens[i].Qualidade <= 0) ? Itens[i].Qualidade  : 0;
                Itens[i].PrazoParaVenda = Itens[i].PrazoParaVenda - 1;

            }
        }
    }
}
